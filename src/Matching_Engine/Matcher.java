package Matching_Engine;

import java.util.LinkedList;
import javax.swing.JOptionPane;



public class Matcher {
	public static LinkedList <MatchingResult> MatchingResultlist = new LinkedList <MatchingResult>(); //int Id, String[] Day, double[] Window, double Dis,String Column

	public static boolean Matching(int sheet_index, String Column_name, String start, String end, String Simirality, boolean s_sw, boolean r_sw){
		double[] data_column = null;      // 전체데이터의 특정 컬럼
		double[] query_column = null;     // 쿼리데이터의 특정 컬럼
		double[] temp_window = null;      // 슬라이딩윈도우 구성시 임시 윈도우
		String[] day = null;              // 1차원 날짜배열
		String[] temp_day = null;         // 임시 날짜배열
		String[][] data = null;           // 데이터 엑셀파일의 시트
		double dis = 0;                    // 유사도
		double QS_SIZE = 0;                // 질의시퀀스의 벡터 사이즈(지수유사도에서 이용)
		int queryrow = 0; //질의엑셀파일의 행의 갯수
			
		//<시트> <컬럼> <startday> <endday> <허용 유사도> <부울린 스케일링> <부울린 반전>
		try {
			data = DataParser.GetSheetToArray(DataParser.workbook, sheet_index);	            // 옵션 구성되어있음
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Sheet명이 잘못되었습니다.");   // Sheet명 오류 검출
			return false;
		}
		
		try {
			data_column = DataParser.converter(data, Column_name);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Column명이 잘못되었습니다.");   // Column명 오류 검출
			return false;
		}
			
		day = DataParser.dayconverter(data);
		
		try {
			query_column = DataParser.SearchSequence(start,end);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "기간이 잘못 되었습니다.");   // 기간 오류 검출
			return false;
		}
		
		QS_SIZE = Sequence_size(se_transformation(query_column));
		
		queryrow = query_column.length; //질의 시퀀스 행의 갯수 구함
		
		temp_window = new double[queryrow];        // 질의 길이만큼 임시 윈도우 공간 생성
		temp_day = new String[queryrow];
		
		//매칭부분 슬라이딩 윈도우
		for (int j = 0; j < data_column.length - queryrow + 1; j ++){  // j+1 은 고유 아이디와 같음
			for (int i = 0; i < queryrow ; i++){
				temp_window[i] = data_column[i+j];  //임시 지수 윈도우 생성 
				temp_day[i] = day[i+j];             //임시 날짜 윈도우 생성
			}
			dis = exponential_similarity(se_matching(query_column,		//질의 시퀀스
										 			 temp_window,		//서브 시퀀스
										 			 s_sw,				//스케일링,리버스 스위치
										 			 r_sw), QS_SIZE); // 윈도우 끼리 매칭후 지수 유사도로 변환
			dis = Double.parseDouble(String.format("%.3f", dis));       // 지수유사도 소수점4번째자리에서 반올림
			try {
				if (Double.parseDouble(Simirality) < dis) MatchingResultlist.add(new MatchingResult(j,copy(temp_window),copy(temp_day),dis,Column_name));				
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "유사허용도가 잘못 되었습니다.");   // 기간 오류 검출
				return false;
			}
		}

		qsort(MatchingResultlist, 0, MatchingResultlist.size() -1); // 유사도순 오름차순 퀵 소팅 (노드 번호가 작을수록 유사도도 작음)
		
		DeBug_MatchingResultlist_show ();
		
		return true;
	}
		
	static double uclidean(double QS[],double SS[]){
		double dis = 0; 
		for(int i=0;i<QS.length;i++){
                dis = dis + (SS[i]-QS[i]) * (SS[i]-QS[i]);
            }		
		return Math.sqrt(dis);
	}
			
	static double se_matching(double QS[],double SS[], boolean scale_sw, boolean reverse_sw){ 
        double QS_SS_multiplied = 0;
        double QS_squared = 0;        
        double dis = uclidean(se_transformation(QS),se_transformation(SS));
		double scale_a;
		double[] Scale_QS = new double [QS.length];
        
        if (scale_sw == true){
			 for(int i=0;i<QS.length;i++){
	                QS_SS_multiplied = QS_SS_multiplied + QS[i]*SS[i];
	                QS_squared = QS_squared + QS[i]*QS[i];
	            }
			 
			 scale_a = QS_SS_multiplied/QS_squared;
			 
			 if(reverse_sw != true)
				 scale_a = Math.abs(scale_a);
			 
			 for(int i=0;i<QS.length;i++) {
				 Scale_QS[i] = QS[i]*scale_a;
			 }
			 dis = uclidean(se_transformation(Scale_QS),se_transformation(SS));
        }         
        return dis;
	}
	
	// SE변환 함수
	static double[] se_transformation(double Sequence[]){		
        double[] Tse_Sequence = new double[Sequence.length];                        // shifted eliminated된 질의시퀀스
        double Sequence_Sum=0;                                    // QS_Sum = 질의시퀀스 각 요소들의 총합, SS_Sum = 서브시퀀스 각 요소들의 총합
        
        /*시퀀스 각 요소들의 합을 구하는 구간*/
        for(int i=0;i<Sequence.length;i++){
        	Sequence_Sum = Sequence_Sum + Sequence[i];
        }

        /*시퀀스 전체 엔트리들로부터 시퀀스의 평균을  빼서 시퀀스를 SE평면상으로 옮기는 과정*/
        for(int i=0;i<Sequence.length;i++){
            Tse_Sequence[i] = Sequence[i]-(Sequence_Sum/Sequence.length);
        }
        
        return Tse_Sequence;
	}
	
	static double max(double a, double b){
		if ( a >  b){
			return a;
		} else {
			return b;
		}
	}
	
	static void swap (LinkedList<MatchingResult> list, int a, int b){
         MatchingResult temp;
         temp = list.get(a);
         list.set(a, list.get(b));
         list.set(b, temp);
     }
	
	 // quick sorting 함수
	static void qsort(LinkedList<MatchingResult> a, int left, int right){
        if(left<right){
        
            int i=0,last=0;
            if (left >= right) return;
        
            last = left;
            for (i=left+1; i <= right; i ++) {
                if(a.get(i).dis < a.get(left).dis ){
                    swap(a, ++last, i);
                }
            }
        
            swap(a, left, last);
            qsort(a, left, last-1);
            qsort(a, left+1, right);                
        }
    }	
	
	// 유클리디언 거리를 지수유사도로 변환하는 함수
	static double exponential_similarity(double dis, double qs_size){
		return 100*(Math.exp(-(dis/qs_size)));
	}
	
	// 유클리디언 거리를 절대유사도로 변환하는 함수
	static double absolute_similarity(double dis, double max_dis){
		return 100*(1-(dis/max_dis));
	}
	
	// 유클리디언 거리를 상대유사도로 변환하는 함수
	static double relative_similarity(double dis, double qs_size){
		return 100*(1-(dis/qs_size));
	}
	
	// 질의시퀀스의 벡터크기를 구하는 함수
	static double Sequence_size(double[] Sequence){
		double size=0;
		for (int i = 0; i<Sequence.length ; i++){
			size = size + (Sequence[i]*Sequence[i]);
		}
		return Math.sqrt(size);
	}
	
	// 질의시퀀스에 대한 최대 유클리디안 거리를 구하는 함수
	static double Max_distance(double[] s){
		double[] Max_ss = new double[s.length];
		double[] Min_ss = new double[s.length];
		
		for(int i=0; i<s.length ; i++){
			Max_ss[i] = s[0]*Math.pow(1.1, i);
			Min_ss[i] = s[0]*Math.pow(0.9, i);
		}
		
		if (uclidean(se_transformation(s),se_transformation(Max_ss)) > uclidean(se_transformation(s),se_transformation(Min_ss))){
			return uclidean(se_transformation(s),se_transformation(Max_ss));
		} else {
			return uclidean(se_transformation(s),se_transformation(Min_ss));
		}
		
	}
	
	static double[] copy(double[] sub){
		double[] temp = new double [sub.length];
		for (int i = 0; i< sub.length ; i++){
			temp[i]=sub[i];
		}
		
		return temp;
	}
	
	static String[] copy(String[] sub){
		String[] temp = new String [sub.length];
		for (int i = 0; i< sub.length ; i++){
			temp[i]=sub[i];
		}
		
		return temp;
	}
	
	// 디버깅 함수 - 링크드리스트 내의 결과 출력
	static void DeBug_MatchingResultlist_show (){
		for (int i = 0; i < MatchingResultlist.size(); i++){			
			System.out.println(MatchingResultlist.get(i).id+" "+MatchingResultlist.get(i).day[0]+" "+MatchingResultlist.get(i).dis);
		}		
	}
}

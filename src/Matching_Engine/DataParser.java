package Matching_Engine;

import java.io.*;
import jxl.*;

public class DataParser {	
	static String data_sheet[][];  // [행][열] 의 고유ID + sheet정보
	public static double query_sheet[][]; // 쿼리 시트의 데이터  
	public static double QUERY[];
	static int column_index;              // data_sheet 배열내에서  사용자 질의컬럼의 위치
	public static Workbook workbook;
	public static String Sheet_name[];

	// 엑셀파일 리드 함수
	public static Workbook Excel_Parsing(File targetFile) throws Exception{
		workbook = Workbook.getWorkbook(targetFile);
		Sheet_name = workbook.getSheetNames();
		return workbook; 
	}

	static String[][] GetSheetToArray(Workbook workbook,int sheet_index){
		Sheet sheet = null;
		sheet = workbook.getSheet(sheet_index);
		
		int rowCount = sheet.getRows();                            //총 행수를 가져옵니다.
		int colCount = sheet.getColumns();                       //총 열의 수를 가져옵니다.
		
		if(rowCount <= 0) {
		    System.err.println("Read 할 데이터가 엑셀에 존재하지 않습니다.");
		    System.exit(1);
		}
			   
		data_sheet = new String[rowCount][colCount + 1]; //고유 ID를 추가하기위한 행추가
		
		data_sheet[2][0] = "ID";
		for(int i = 0; i < rowCount-3 ; i++){
		   data_sheet[i+3][0] = Integer.toString(i);           // [4][0],4행부터 0번째 열에 고유 ID추가 
		}
			   
		//엑셀데이터를 배열에 저장
		for(int i = 0 ; i < rowCount ; i++) {
			for(int k = 0 ; k < colCount ; k++) {
			     Cell cell =sheet.getCell(k, i);                   // 해당위치의 셀을 가져오는 부분입니다.
			     if(cell == null) continue;
			     data_sheet[i][k+1] = cell.getContents();              // 가져온 셀의 실제 콘텐츠 즉 데이터(문자열)를 가져오는 부분입니다.
			}
		}
		
		// 배열 검출부
		
//		for(int r=0 ; r<data_sheet.length ; r++) {
//		    for(int c=0 ; c<data_sheet[0].length ; c++) {
//		     System.out.print(data_sheet[r][c]+" ");
//		    }
//		    System.out.println();
//		   }				
		return data_sheet;
	}
		
	//엑셀파일의 원하는 항목을 1차원 배열로 반환하는 함수
	static double[] converter(String data[][], String colname){
		
		double Data[] = new double[data.length-3];
						
		for (int k = 0; k < data[2].length ; k++){    // 해당 컬럼 찾는 구간
			if(colname.equals(data[2][k])){
				column_index = k;
				break;
			}
		}
					
		for(int i = 0 ; i < data.length-3 ; i++){
			Data[i] = Double.parseDouble(data[3+i][column_index]);        //해당컬럼을 1차원 배열에 입력
		}
				
		return Data;
	}
	
	static String[] dayconverter(String data[][]){ // 날짜컬럼 일차원배열로 반환
		int colnum = 1; // 날짜 컬럼의 위치
		String Data[] = new String[data.length-3];
						
		for(int i = 0 ; i < data.length-3 ; i++){
			Data[i] = data[i][colnum];
		}
				
		return Data;
	}
	
	static double[] SearchSequence(String start,String end){
		int start_id = -1;
		int end_id = -1;
		
		
		for(int i = 0 ; i < data_sheet.length ; i++){
			if (start.equals(data_sheet[i][1])){
				start_id = i;
			}
			if (end.equals(data_sheet[i][1])){
				end_id = i;
			}
		}
		
		QUERY = new double[end_id-start_id+1];
		
		for(int i = 0; i < QUERY.length; i++){
			QUERY[i] = Double.parseDouble(data_sheet[start_id + i +3][column_index]);
			
		}
		return QUERY;
	}
	
	// DataParser 디버깅 main함수
//	public static void main(String[] args) {
//		try {
//			ExcelRead(new File("Data2.xls"), "Sheet1");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}

}

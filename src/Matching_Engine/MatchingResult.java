package Matching_Engine;

public class MatchingResult{

	    public int id;                      // 윈도우의 시작 ID
		public double[] window = null;      // 윈도우 내용
		String[] day = null;         // 해당지수의 날짜
		public double dis;                  // 윈도우와 쿼리간의 유사도
		String column;               // 윈도우가 속한 컬럼명

		MatchingResult(int Id, double[] Window, String[] Day, double Dis,String Column){
			id = Id;
			dis = Dis;
			window = Window;
			day = Day;
			column = Column;
		}
}
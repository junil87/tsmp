package UserInterface;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Matching_Engine.DataParser;

import jxl.Cell;
import jxl.Sheet;

public class Matching_dialog extends JDialog{
	Container contentPane;
	JLabel lb1 = new JLabel(" 질의파일 경로");	
	JLabel lb3 = new JLabel(" Sheet : ");
	JLabel lb4 = new JLabel(" Column :");
	JLabel lb5 = new JLabel(" 유사허용도  :");
	JLabel lb8 = new JLabel(" 기간 입력    ");
	JLabel lb9 = new JLabel("           ~ ");
	JTextField path_query = new JTextField("우상일자형.xls");
	JTextField start = new JTextField("2004/04/22");
	JTextField end = new JTextField("2004/06/02");
	JTextField sheet = new JTextField("Sheet1");
	JTextField column = new JTextField("현재지수");
	JTextField simirality = new JTextField("유사허용도 (%)");
	JCheckBox scaling = new JCheckBox("스케일링");
	JCheckBox reverse = new JCheckBox("반전");
	JButton btnOK = new JButton("확인");
	JButton btnCANCLE = new JButton("취소");
	JComboBox<String> Columnlist;
	JComboBox<String> Sheetlist;
	JPanel r_1;
	JPanel r_2;
	
		public Matching_dialog(int sw){
			contentPane = this.getContentPane();
			this.setLayout(null);
			
			Sheetlist = new JComboBox<String>(DataParser.Sheet_name);
			Sheetlist.setBounds(0,0,100,20);
			Columnlist = new JComboBox<String>(); //초기화
			Columnlist = Makecombobox(0);
			Columnlist.setBounds(0,33,100,20);
			simirality.setBounds(0, 67, 100, 20);
			
			r_1 = new JPanel(new GridLayout (3,1,0,10));
    		r_1.add(lb3);
    		r_1.add(lb4);
    		r_1.add(lb5);
    		
    		r_2 = new JPanel();
    		r_2.setLayout(null);
    		r_2.add(Sheetlist);
    		r_2.add(Columnlist);
    		r_2.add(simirality);
    		
    		JPanel option = new JPanel(new GridLayout (3,1,0,10));
    		option.add(scaling);
    		option.add(reverse);
    		
    		r_1.setBounds(20, 20, 80, 90);
    		r_2.setBounds(110, 20, 120, 90);
    		option.setBounds(250,20,110,60);
    		btnOK.setBounds(92,220,70,30);
    		btnCANCLE.setBounds(172,220,70,30);
    		
    		//스위치 변수에 따라 inner,file 매칭 다이얼로그 변수를 띄워줌
    		JPanel h_3 = new JPanel(new GridLayout (1,4,0,10));
			h_3.add(lb8);
			h_3.add(start);			
			h_3.add(lb9);
			h_3.add(end);
			h_3.setBounds(20, 120, 300, 30); 
			contentPane.add(h_3);			
			contentPane.add(r_1);
			contentPane.add(r_2);
			contentPane.add(option);
			contentPane.add(btnOK);
			contentPane.add(btnCANCLE);
            this.setSize(350,300);
		}
		
		
		static JComboBox<String> Makecombobox(int index){
			Sheet sheet = DataParser.workbook.getSheet(index);
			String list[] = new String[sheet.getColumns()];
			for (int i = 0; i < sheet.getColumns(); i++){
				Cell cell = sheet.getCell(i, 2);
				list[i] = cell.getContents();
			}
			return new JComboBox<String> (list);
		}
}



package UserInterface;

import javax.swing.*;  

import Matching_Engine.DataParser;
import Matching_Engine.Matcher;

import java.awt.GridLayout;
import java.awt.event.*;
import java.io.File;

import jxl.Cell;
import jxl.Sheet;

public class Application{
	static JFrame frame = new JFrame("SE Matching program");
	static JTable table;
	static JScrollPane scroll;
	static JComboBox<String> Sheetlist;
	static JPanel main = new JPanel();
	static String Sheet_name[];
	static JLabel Sheetname = new JLabel("Sheet 명 :");
	static JLabel Excelfilename;
	static JPanel combopanel = new JPanel(new GridLayout (1,2,0,10));
	static File file;  //읽어들인 데이터셋 정보
	static Matching_dialog matching_dialog;
	static Chart_dialog chart_dialog;
	static boolean sw = false;
	
	static ActionListener open = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			// 파일 오픈 버튼 눌럿을시 기본폴더지정, 없을경우 디플토
			JFileChooser chooser = new JFileChooser("C:\\Users\\jun\\Documents\\자바프로젝트\\SE_ver2");			
			int choice = chooser.showOpenDialog(frame);
			if (choice == JFileChooser.APPROVE_OPTION){
				file = chooser.getSelectedFile();
				
				//파일 오픈 예외처리
				try {
					DataParser.Excel_Parsing(file);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "파일이 잘못 되었습니다.");
					return;
				}
				// 파일오픈 여부에따라 시트 구성				
				if ( sw == true ) Framereset();   // 패널초기화 코드
				MakeExcelFrame();	//메인프레임 구성			
			}
		}		
	};
	
	static ActionListener SHEETLIST = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			main.remove(scroll);
						
			table = MakeJTable(Sheetlist.getSelectedIndex());
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			scroll = new JScrollPane(table);
			scroll.setBounds(0, 40, 987, 900);
			
			main.add(scroll);
			frame.repaint();
		}
	};
	
	//다이얼로그 캔슬 이벤트
	static ActionListener CANCLE = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			matching_dialog.setVisible(false);
		}
	};
	
	//exit메뉴 이벤트
	static ActionListener exit = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			System.exit(0);
		}
	};
	
	//inner매칭 다이얼로그 이벤트
	static ActionListener MATCHING = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			if (file == null){
				JOptionPane.showMessageDialog(null, "우선 파일을 열어주세요");
			} else {
				matching_dialog = new Matching_dialog(1);
				matching_dialog.btnCANCLE.addActionListener(CANCLE);
				matching_dialog.btnOK.addActionListener(MATCHING_OK);
				matching_dialog.Sheetlist.addActionListener(Matching_dialog_SHEETLIST);
				matching_dialog.setTitle("matching");
				matching_dialog.setVisible(true);
			}
		}
	};
	
	//매칭 다이얼로그 OK버튼 이벤트 (매칭 start이벤트)
	static ActionListener MATCHING_OK = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			if(Matcher.MatchingResultlist.isEmpty() == false) Matcher.MatchingResultlist.clear();
//			String[] args = {file.toString(), matching_dialog.Sheetlist.getSelectedItem().toString(), matching_dialog.Columnlist.getSelectedItem().toString(), 
//					         matching_dialog.start.getText(), matching_dialog.end.getText(), matching_dialog.simirality.getText()};
			if (Matcher.Matching(matching_dialog.Sheetlist.getSelectedIndex(),
								 matching_dialog.Columnlist.getSelectedItem().toString(),
								 matching_dialog.start.getText(),
								 matching_dialog.end.getText(),
								 matching_dialog.simirality.getText(),
								 matching_dialog.scaling.isSelected(), 
								 matching_dialog.reverse.isSelected()) == true){
				matching_dialog.setVisible(false);
				chart_dialog = new Chart_dialog();
				chart_dialog.setTitle("Result chart");
				chart_dialog.combolist.addActionListener(simirality_list);
				chart_dialog.setVisible(true);
			}				
		}
	};
	
	// 챠트다이얼로그 콤보박스 이벤트	
	static ActionListener simirality_list = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			chart_dialog.contentPane.remove(chart_dialog.chart);
			chart_dialog.chart = chart_dialog.MakeChart(chart_dialog.combolist.getSelectedIndex());
			chart_dialog.chart.setBounds(10, 40, 700, 500);			
			chart_dialog.contentPane.add(chart_dialog.chart);
			chart_dialog.repaint();
		}
	};

	static ActionListener Matching_dialog_SHEETLIST = new ActionListener(){
		public void actionPerformed (ActionEvent e){
			matching_dialog.r_2.remove(matching_dialog.Columnlist);
			matching_dialog.Columnlist = matching_dialog.Makecombobox(matching_dialog.Sheetlist.getSelectedIndex());
			matching_dialog.Columnlist.setBounds(0,33,100,20);
			matching_dialog.r_2.add(matching_dialog.Columnlist);
			//matching_dialog.r_2.repaint();
			//matching_dialog.Columnlist.repaint(0,33,100,20);
		}
	};
	
	static void createAndShowGUI(){
		JMenuBar MenuBar = new JMenuBar();
		JMenu File = new JMenu("File");
		JMenu Matching = new JMenu("Matching");
		JMenu Help = new JMenu("Help");
		
		JMenuItem Open = new JMenuItem("Open");
		Open.addActionListener(open);
		JMenuItem Exit = new JMenuItem("Exit");
		Exit.addActionListener(exit);
		
		JMenuItem matching = new JMenuItem("Matching");
		matching.addActionListener(MATCHING);
		
		//작업메뉴바의 아이템 추가
		MenuBar.add(File);
	    MenuBar.add(Matching);
	    MenuBar.add(Help);
	    
	    //각 메뉴에 아이템 추가
	    File.add(Open);
	    File.add(Exit);
	    Matching.add(matching);
	    
	    frame.setSize(1000, 1000);
		frame.setJMenuBar(MenuBar);
		frame.setVisible(true);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run(){
				createAndShowGUI();
			}
		});
	}


	static JTable MakeJTable(int sheetindex){
		Sheet sheet = DataParser.workbook.getSheet(sheetindex);
		Object row[][] = new String[sheet.getRows()][sheet.getColumns()];
		Object Columns[] = new String[sheet.getColumns()];
		
		for (int i = 0; i < sheet.getColumns(); i++){
			Cell cell = sheet.getCell(i, 2);
			Columns[i] = cell.getContents();
		}
		
		
		for (int j = 0; j <sheet.getRows()-3; j++){
			for (int i = 0; i < sheet.getColumns(); i++){
				Cell cell = sheet.getCell(i, 3 + j);
				row[j][i] = cell.getContents();
			}
		}
		
		return new JTable(row,Columns);
	}
		
	//메인 프레임 초기화 함수
	static void Framereset(){
		combopanel.remove(Excelfilename);
		combopanel.remove(Sheetname);
		combopanel.remove(Sheetlist);
		main.remove(combopanel);
		main.remove(scroll);
		frame.remove(main);
	}
	
	static void MakeExcelFrame(){
		main.setLayout(null);
		Excelfilename = new JLabel("File 명 : " + file.getName());
		Sheet_name = DataParser.workbook.getSheetNames();
		Sheetlist = new JComboBox<String>(Sheet_name);
		Sheetlist.addActionListener(SHEETLIST);
		table = MakeJTable(0);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); //테이블 가로스크롤 구현
		scroll = new JScrollPane(table);
			
		combopanel.setLayout(null);
		combopanel.add(Excelfilename);
		combopanel.add(Sheetname);
		combopanel.add(Sheetlist);	     			

		Excelfilename.setBounds(0, 0, 150, 20);
		Sheetname.setBounds(155, 0, 60, 20);
		Sheetlist.setBounds(220, 0, 200, 20);
		combopanel.setBounds(10, 10, 500, 20);
		scroll.setBounds(0, 40, 987, 900);
 			
		main.add(combopanel);
		main.add(scroll);
		frame.add(main);
		frame.setVisible(true);
		frame.repaint();
		sw = true;
	}

}

package UserInterface;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import Matching_Engine.DataParser;
import Matching_Engine.Matcher;

public class Chart_dialog extends JDialog {

	Container contentPane;
	static JComboBox<String> combolist;
	static ChartPanel chart;
	JLabel lb1 = new JLabel(" 유사도  :");
	String dis[];
	
	public Chart_dialog(){
		contentPane = this.getContentPane();
		this.setLayout(null);
		dis = new String[Matcher.MatchingResultlist.size()];
				
		for(int i = 0; i < Matcher.MatchingResultlist.size() ; i++){
			dis[i] = Double.toString(Matcher.MatchingResultlist.get(i).dis);
		}
		
		combolist = new JComboBox<String>(dis);
		JPanel r_1 = new JPanel(new GridLayout (1,2,0,10));
		
		chart = MakeChart(0);
		chart.setBounds(10, 40, 700, 500);

		r_1.add(lb1);
		r_1.add(combolist);
		r_1.setBounds(10, 10, 150, 20);
		contentPane.add(r_1);
		contentPane.add(chart);
		this.setSize(750,700);
	}
			
    // result클래스의 인덱스를 기반으로 챠트생성 함수
	ChartPanel MakeChart (int index){
		XYSeries series1 = new XYSeries("Query");
		for(int i = 0 ; i < DataParser.QUERY.length ; i++){
			series1.add(Matcher.MatchingResultlist.get(index).id + i, DataParser.QUERY[i]);
		}
		
		XYSeries series2 = new XYSeries("Sub");
		for(int i = 0 ; i < DataParser.QUERY.length ; i++){
			series2.add(Matcher.MatchingResultlist.get(index).id + i, Matcher.MatchingResultlist.get(index).window[i]);
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        
        ////////////////////////////////////////////////////////////////////
        final JFreeChart chart = ChartFactory.createXYLineChart(
	            "Matching Result",      // chart title
	            "X",                      // x axis label
	            "Y",                      // y axis label
	            dataset,                  // data
	            PlotOrientation.VERTICAL,
	            true,                     // include legend
	            true,                     // tooltips
	            false                     // urls
	        );

	        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
	        chart.setBackgroundPaint(Color.white);

//	        final StandardLegend legend = (StandardLegend) chart.getLegend();
	  //      legend.setDisplaySeriesShapes(true);
	        
	        // get a reference to the plot for further customisation...
	        final XYPlot plot = chart.getXYPlot();
	        plot.setBackgroundPaint(Color.lightGray);
	    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
	        plot.setDomainGridlinePaint(Color.white);
	        plot.setRangeGridlinePaint(Color.white);
	        
	        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
	        //renderer.setSeriesLinesVisible(0, false);  // 라인 보이는지 여부
	        //renderer.setSeriesShapesVisible(1, false); // 포인트 보이는지 여부
	        // 칼라설정
	        renderer.setSeriesPaint(0, Color.black);
	        renderer.setSeriesPaint(1, Color.RED);
	        plot.setRenderer(renderer);

	        // change the auto tick unit selection to integer units only...
	        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
	        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
	        // OPTIONAL CUSTOMISATION COMPLETED.
	        
        
		return new ChartPanel(chart);		
	}
}